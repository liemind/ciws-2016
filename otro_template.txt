<http://interior.cl/funcionario#%0%> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Person> 
<http://interior.cl/funcionario#%0%> <http://purl.org/dc/terms/source> <http://www.interior.gob.cl>
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/trabaja_en> "subsecretaria del interior"
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/estamento> "%1%" 
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/apellidoPaterno> "%2%" 
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/apellidoMaterno> "%3%" 
<http://interior.cl/funcionario#%0%> <http://xmlns.com/foaf/0.1/givenName> "%4%" 
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/grado> "%5%" 
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/formacion> "%6%" 
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/cargo> "%7%" 
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/region> "%8%" 
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/unidadMonetaria> "%10%" 
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/remuBruta> "%11%"^^xsd:integer 
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/horasExtraordinarias> "%12%" 
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/fechaInicio> "%13%"^^xsd:dateTime 
<http://interior.cl/funcionario#%0%> <http://transparencia.cl/voc/fechaTermino> "%14%"